import { Component, OnInit } from '@angular/core';
import { Student } from '../student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-studentbyid',
  templateUrl: './studentbyid.component.html',
  styleUrls: ['./studentbyid.component.css']
})
export class StudentbyidComponent implements OnInit {
  public students:Student[]=[];
  public id:number;
  public student: Student;
  constructor(public service:StudentService) { }

 public getStudentsById(){
 this.students=[];
 this.service.getStudentsById(this.id).subscribe(
 data=>{
   this.student=data;
   this.students.push(this.student);
 }
);}

  ngOnInit() {
      }

}
