import { Student } from './student';
import { Subject } from './subject';


export class Teacher {

    teacherId:number;
    teacherName:string;
    teacherAddress:string;
    teacherPhone:number;
    studentList:Student[];
    subjectList:Subject[];
}


