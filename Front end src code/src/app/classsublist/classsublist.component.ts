import { Component, OnInit } from '@angular/core';
import { Classes } from '../classes';
import { Subject } from '../subject';
import { ClassService } from '../class.service';
import { SubjectService } from '../subject.service';

@Component({
  selector: 'app-classsublist',
  templateUrl: './classsublist.component.html',
  styleUrls: ['./classsublist.component.css']
})
export class ClasssublistComponent implements OnInit {

  public class:Classes[];
  public subjects:Subject[];
  constructor(public service:ClassService,public stuservice:SubjectService) { }

  public studetatils(){
  this.stuservice.getAllSubjects().subscribe(data=>{
    this.subjects=data;
  });
 }


  ngOnInit() {
    this.service.getAllClass().subscribe(data=>{
      this.class=data;
      console.log(this.class);
    });
    
  }
}
