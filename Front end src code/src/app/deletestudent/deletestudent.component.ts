import { Component, OnInit } from '@angular/core';
import { Student } from '../student';
import { StudentService } from '../student.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-deletestudent',
  templateUrl: './deletestudent.component.html',
  styleUrls: ['./deletestudent.component.css']
})
export class DeletestudentComponent implements OnInit {

  public students:Student[];//=[];
  public student:Student;
  public id:number;
  constructor(public service:StudentService, public router:Router) { }

  public deleteStudent(){

    return this.service.deleteStudent(this.id).subscribe(
      data=>{
        this.student=data;
        this.students.pop;
        this.router.navigate(['/StudentList'])
      }
    )

  }

  ngOnInit() {
    this.service.getAllStudents().subscribe(data=>{
      this.students=data;
    });
  }

}
