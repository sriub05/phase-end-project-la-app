export class Student {
    studentId:number;
    studentName:string;
    studentAddress:string;
    studentEmergencycontact: string;
    studentPhone:number;
}
