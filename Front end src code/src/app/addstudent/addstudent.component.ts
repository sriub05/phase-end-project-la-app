import { Component, OnInit } from '@angular/core';
import { Student } from '../student';
import { StudentService } from '../student.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addstudent',
  templateUrl: './addstudent.component.html',
  styleUrls: ['./addstudent.component.css']
})
export class AddstudentComponent  {
public student:Student
  constructor(public service:StudentService,public router:Router) { 
    this.student=new Student();
    }

    createStudent(){
      console.log(this.student);
      this.service.createStudent(this.student).subscribe(data=>{
        this.student=new Student();
          this.router.navigate(['/studentList'])
      })
    }
  

}
