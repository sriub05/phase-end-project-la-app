import { Component, OnInit } from '@angular/core';
import { Classes } from '../classes';
import { ClassService } from '../class.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-deleteclass',
  templateUrl: './deleteclass.component.html',
  styleUrls: ['./deleteclass.component.css']
})
export class DeleteclassComponent implements OnInit {
  public c:Classes[]=[];
  public class:Classes;
  public id:number;
  
  constructor(public service:ClassService,public router:Router) { }

   public deleteClass(){
    return this.service.deleteClass(this.id).subscribe(
      data=>{
        this.class=data;
        this.c.pop;
        this.router.navigate(['/classlist'])
      }
    )

   }
  ngOnInit() {
    this.service.getAllClass().subscribe(data=>{
      this.c=data;
      console.log(this.class);
    });
  }

}
