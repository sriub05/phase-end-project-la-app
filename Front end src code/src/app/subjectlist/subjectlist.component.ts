import { Component, OnInit } from '@angular/core';
import { Subject } from '../subject';
import { SubjectService } from '../subject.service';

@Component({
  selector: 'app-subjectlist',
  templateUrl: './subjectlist.component.html',
  styleUrls: ['./subjectlist.component.css']
})
export class SubjectlistComponent implements OnInit {
  public subjects:Subject[];
  constructor(public service:SubjectService) { }

  ngOnInit() {
    this.service.getAllSubjects().subscribe(data=>{
      this.subjects=data;
    });
  }

}
