import { Component, OnInit } from '@angular/core';
import { Teacher } from '../teacher';
import { TeacherService } from '../teacher.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-updateteacher',
  templateUrl: './updateteacher.component.html',
  styleUrls: ['./updateteacher.component.css']
})
export class UpdateteacherComponent implements OnInit {
  public t:Teacher;
  public teacher:Teacher[]=[];
  constructor(public service:TeacherService,public route:Router) { 
    this.t=new Teacher();  }

public updateTeacher(){
  return this.service.updateTeacher(this.t).subscribe(
    data=>{this.t=new Teacher();
    this.route.navigate(['/teacherlist'])}
  )
}

  ngOnInit() {
    this.service.getAllTeacher().subscribe(data=>{
      this.teacher=data;
    });
  }

}
