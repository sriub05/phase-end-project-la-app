import { Component, OnInit } from '@angular/core';
import { Teacher } from '../teacher';
import { TeacherService } from '../teacher.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-deleteteacher',
  templateUrl: './deleteteacher.component.html',
  styleUrls: ['./deleteteacher.component.css']
})
export class DeleteteacherComponent implements OnInit {

  public teacher:Teacher[]=[];
  public t:Teacher;
  public id:number;

  constructor(public service:TeacherService,public router:Router) { }

  public deleteTeacher(){
    return this.service.deleteTeacher(this.id).subscribe(
      data=>{
        this.t=data;
        this.teacher.pop;
        this.router.navigate(['/teacherlist'])

      }
    )
  }
  ngOnInit() {
  }

}
