import { Component, OnInit } from '@angular/core';
import { Subject } from '../subject';
import { SubjectService } from '../subject.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addsubject',
  templateUrl: './addsubject.component.html',
  styleUrls: ['./addsubject.component.css']
})
export class AddsubjectComponent  {
public subject:Subject
  constructor(public service:SubjectService,public router:Router) { 
    this.subject=new Subject();
    }

    createSubject(){
      console.log(this.subject);
      this.service.createSubject(this.subject).subscribe(data=>{
        this.subject=new Subject();
          this.router.navigate(['/SubjectList'])
      })
    }
  

}
