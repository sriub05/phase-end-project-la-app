import { Component, OnInit } from '@angular/core';
import { Subject } from '../subject';
import { SubjectService } from '../subject.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-deletesubject',
  templateUrl: './deletesubject.component.html',
  styleUrls: ['./deletesubject.component.css']
})
export class DeletesubjectComponent implements OnInit {

  public subjects:Subject[];//=[];
  public subject:Subject;
  public id:number;
  constructor(public service:SubjectService, public router:Router) { }

  public deleteSubject(){

    return this.service.deleteSubject(this.id).subscribe(
      data=>{
        this.subject=data;
        this.subjects.pop;
        this.router.navigate(['/SubjectList'])
      }
    )

  }

  ngOnInit() {
    this.service.getAllSubjects().subscribe(data=>{
      this.subjects=data;
    });
  }

}
