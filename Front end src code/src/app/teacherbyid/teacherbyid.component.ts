import { Component, OnInit } from '@angular/core';
import { TeacherService} from '../teacher.service'
import { Teacher } from '../teacher';
@Component({
  selector: 'app-teacherbyid',
  templateUrl: './teacherbyid.component.html',
  styleUrls: ['./teacherbyid.component.css']
})
export class TeacherbyidComponent implements OnInit {
  public teacher:Teacher[]=[];
  public t:Teacher;
  public id:number;
  constructor(public service:TeacherService ){ }


  public getTeacherById(){
    this.teacher=[];
    this.service.getTeacherById(this.id).subscribe(
    data=>{
      this.t=data;
      this.teacher.push(this.t);
    }
   );}
  ngOnInit() {
  }

}
