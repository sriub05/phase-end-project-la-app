import { Component, OnInit } from '@angular/core';
 import {ClassService} from '../class.service'
import { Classes } from '../classes';
@Component({
  selector: 'app-classbyid',
  templateUrl: './classbyid.component.html',
  styleUrls: ['./classbyid.component.css']
})
export class ClassbyidComponent implements OnInit {
  public classes:Classes[]=[];
  public id:number;
  public class: Classes;


  constructor(public service:ClassService) { }

  public getClassById(){
    this.classes=[];
    this.service.getClassById(this.id).subscribe(
    data=>{
      this.class=data;
      this.classes.push(this.class);
    }
   );}

  ngOnInit() {
  }

}
