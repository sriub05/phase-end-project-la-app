import { Component, OnInit } from '@angular/core';
import { Classes } from '../classes';
import { ClassService } from '../class.service';
import { Router } from '@angular/router';
import {ClasslistComponent} from '../classlist/classlist.component';
import { Student } from '../student';
import {StudentService} from '../student.service';

@Component({
  selector: 'app-updatestuclass',
  templateUrl: './updatestuclass.component.html',
  styleUrls: ['./updatestuclass.component.css']
})
export class UpdatestuclassComponent implements OnInit {
  public C:Classes;
  public class:Classes[]=[];
  public student: Student;
  public students: Student[];




  constructor(public service:ClassService,public route:Router) { 
    this.C=new Classes();
    this.student = new Student();
    }
  
  public updatestuClass(){
    
    return this.service.updatestuClass(this.C).subscribe(
      data=>{this.C=new Classes();this.student=new Student();

      //this.route.navigate(['/classlist'])
    }
    )
  }

  ngOnInit() {
    this.service.getAllClass().subscribe(data=>{
      this.class=data;
      console.log(this.class);
      
    });
  }

}
