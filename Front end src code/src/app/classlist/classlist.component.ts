import { Component, OnInit } from '@angular/core';
import { Classes } from '../classes';
import { ClassService } from '../class.service';
import {StudentService} from '../student.service';
import {Student} from '../student';


@Component({
  selector: 'app-classlist',
  templateUrl: './classlist.component.html',
  styleUrls: ['./classlist.component.css']
})
export class ClasslistComponent implements OnInit {
  public class:Classes[];
  public students:Student[];
  constructor(public service:ClassService,public stuservice:StudentService) { }

  public studetatils(){
  this.stuservice.getAllStudents().subscribe(data=>{
    this.students=data;
  });
 }


  ngOnInit() {
    this.service.getAllClass().subscribe(data=>{
      this.class=data;
      console.log(this.class);
    });
    
  }

}
